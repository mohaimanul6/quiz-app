import classes from "../styles/From.module.css";

function Form({ children, className, ...rest }) {
  console.log(`${classes.form}`);
  return (
    <form className={`${className} ${classes.form}`} {...rest}>
      {children}
    </form>
  );
}

export default Form;
