import React from "react";
import Answers from "./Answers";

export default function Questions({ classes, answers = [] }) {
  console.log(classes);
  return answers.map((answers, index) => (
    <div className={classes.question} key={index}>
      <div className={classes.qtitle}>
        <span className="material-icons-outlined"> help_outline </span>
        {answers.title}
      </div>
      <Answers input={false} options={answers.options} />
    </div>
  ));
}
