import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  // <React.StrictMode>
  // </React.StrictMode>,
  <App />,
  document.getElementById("root")
);

reportWebVitals();
