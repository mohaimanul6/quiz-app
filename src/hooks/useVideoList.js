import {
  get,
  getDatabase,
  limitToFirst,
  orderByKey,
  query,
  ref,
  startAt,
} from "firebase/database";
import { useEffect, useState } from "react";

export default function useVideoList(page) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(true);
  const [videos, setVideos] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  useEffect(() => {
    async function fetchVideos() {
      const db = getDatabase();
      const videoRef = ref(db, "videos");
      const videoQuiry = query(
        videoRef,
        orderByKey(),
        startAt("" + page),
        limitToFirst(8)
      );
      try {
        setError(false);
        setLoading(true);
        const snapshot = await get(videoQuiry, orderByKey);
        setLoading(false);
        if (snapshot.exists()) {
          setVideos((prevVideo) => {
            return [...prevVideo, ...Object.values(snapshot.val())];
          });
        } else {
          setHasMore(false);
        }
      } catch (e) {
        console.log(e);
        setLoading(false);
        setError(true);
      }
    }
    setTimeout(() => {
      fetchVideos();
    }, 1000);
    // return () => {
    //     cleanup
    // }
  }, [page]);
  return { loading, error, videos, hasMore };
}
