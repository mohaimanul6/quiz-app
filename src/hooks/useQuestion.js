import { get, getDatabase, orderByKey, query, ref } from "firebase/database";
import { useEffect, useState } from "react";

export default function useQuestion(videoId) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(true);
  const [questions, setQuestions] = useState([]);
  useEffect(() => {
    async function fetchQuestions() {
      const db = getDatabase();
      const quizRef = ref(db, "quiz/" + videoId + "/questions");
      const quizQuiry = query(quizRef, orderByKey());
      try {
        setError(false);
        setLoading(true);
        const snapshot = await get(quizQuiry, orderByKey);
        setLoading(false);
        if (snapshot.exists()) {
          setQuestions((prevQuestion) => {
            return [...prevQuestion, ...Object.values(snapshot.val())];
          });
        }
      } catch (e) {
        console.log(e);
        setLoading(false);
        setError(true);
      }
    }
    setTimeout(() => {
      fetchQuestions();
    }, 1000);
    // return () => {
    //     cleanup
    // }
  }, [videoId]);
  return { loading, error, questions };
}
